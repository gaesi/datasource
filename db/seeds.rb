# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

# Register real budget from ABC's SIAFI
puts 'Inserting ABC SIAFI budget...'.colorize(:light_blue)
unless Rails.env.test?
  # Insertions::ABCBudget.instance.load_and_persist
end
puts '...ABC projects inserted.'.colorize(:green)
# /BUDGET ABC SIAFI

# Register real BNAFAR Dispensation
puts 'Inserting BNAFAR Dispensation...'.colorize(:light_blue)
unless Rails.env.test?
  #Time Spent: started at 2019-10-15 17:07:44 -0300 finished at 2019-10-15 17:31:50
  Insertions::HEspBNAFARDispensation.instance.load_and_persist(show_progress=true, pagination=30000) #, pagination=350000
  Insertions::HBaseStratBNAFARDispensation.instance.load_and_persist(show_progress=true, pagination=900000) #, pagination=350000
  Insertions::WsBNAFARDispensation.instance.load_and_persist(show_progress=true, pagination=900000) #, pagination=350000
end
puts '...BNAFAR Dispensation projects inserted.'.colorize(:green)
# /BNAFAR Dispensation

# Register real BNAFAR Remedies
puts 'Inserting BNAFAR Remedy...'.colorize(:light_blue)
unless Rails.env.test?
  # Insertions::BNAFARRemedy.instance.load_and_persist
end
puts '...BNAFAR Dispensation projects inserted.'.colorize(:green)
# /Register real BNAFAR Remedies
