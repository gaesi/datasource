require "rails_helper"

RSpec.describe BnafarRemediesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/bnafar_remedies").to route_to("bnafar_remedies#index")
    end

    it "routes to #show" do
      expect(:get => "/bnafar_remedies/1").to route_to("bnafar_remedies#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/bnafar_remedies").to route_to("bnafar_remedies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/bnafar_remedies/1").to route_to("bnafar_remedies#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/bnafar_remedies/1").to route_to("bnafar_remedies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/bnafar_remedies/1").to route_to("bnafar_remedies#destroy", :id => "1")
    end
  end
end
