class BnafarDispensatioNsController < ApplicationController
  before_action :set_bnafar_dispensation, only: [:show, :update, :destroy]

  # GET /bnafar_dispensations
  def index
    @bnafar_dispensations = BnafarDispensation.all

    render json: @bnafar_dispensations
  end

  # GET /bnafar_dispensations/1
  def show
    render json: @bnafar_dispensation
  end

  # POST /bnafar_dispensations
  def create
    @bnafar_dispensation = BnafarDispensation.new(bnafar_dispensation_params)

    if @bnafar_dispensation.save
      render json: @bnafar_dispensation, status: :created, location: @bnafar_dispensation
    else
      render json: @bnafar_dispensation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /bnafar_dispensations/1
  def update
    if @bnafar_dispensation.update(bnafar_dispensation_params)
      render json: @bnafar_dispensation
    else
      render json: @bnafar_dispensation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /bnafar_dispensations/1
  def destroy
    @bnafar_dispensation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bnafar_dispensation
      @bnafar_dispensation = BnafarDispensation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def bnafar_dispensation_params
      params.require(:bnafar_dispensation).permit(:NU_COMPETENCIA_DISPENSACAO, :NU_CATMAT, :CO_PRINCIPIO_ATIVO_MEDICAMENTO, :VL_UNITARIO, :VL_UNITARIO_MIN, :VL_UNITARIO_MAX, :VL_UNITARIO_MEDIAN, :MONTANTE_DISPENSADA, :CO_GRUPO_FINANCIAMENTO, :SG_PROGRAMA_SAUDE, :CO_MUNICIPIO_IBGE_EST, :COORD, :ORIGIN)
    end
end
