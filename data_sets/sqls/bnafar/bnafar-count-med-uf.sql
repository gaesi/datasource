SELECT SUBSTRING(CO_MUNICIPIO_IBGE_EST, 1, 2) AS CO_UF, count(*) #* count(*)
FROM datasource_development.bnafar_dispensations
#Mesma UF (Paciente e Estabelecimento) = 1256928, Total = 13749999 --> LEFT(CO_MUNICIPIO_IBGE_EST, 2) LIKE LEFT(CO_MUNICIPIO_IBGE_PAC, 2);
#Mesmo Município = 6503 --> CO_MUNICIPIO_IBGE_EST LIKE CO_MUNICIPIO_IBGE_PAC;
GROUP BY CO_UF;