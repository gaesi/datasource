require 'singleton'

module Insertions
  class WsBNAFARDispensation < Facade
    include Singleton

    def initialize
      self.model = 'BNAFARDispensation'
      self.file_name = 'wsbnafar-consolidado.csv'
      self.additional_column = ['ORIGIN']
      self.additional_val = ['w']
    end
  end
end
