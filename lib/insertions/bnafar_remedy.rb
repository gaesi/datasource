require 'singleton'

module Insertions
  class BNAFARRemedy < Facade
    include Singleton

    def initialize
      self.model = 'BNAFARRemedy'
      self.file_name = 'tbs_bnafar_root/horus.produto.csv'
      self.specific_attrs = %w(DS_PRODUTO NU_CATMAT CO_GRUPO_FINANCIAMENTO)
    end
  end
end
